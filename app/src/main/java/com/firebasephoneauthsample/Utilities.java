package com.firebasephoneauthsample;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;
import android.widget.TextView;

public class Utilities
{
    public static Dialog dialog;

    public static void showProgressDialog(Context cntxt, String strMsg) {
        if (dialog != null) {
            hideProgressDialog();
        }

        dialog = new Dialog(cntxt);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.wellthy_progress_bar);
        TextView tvMsg = (TextView) dialog.findViewById(R.id.txtvwLoadingMessage);
        tvMsg.setText(strMsg);

        dialog.setCancelable(false);
        dialog.show();
    }


    public static void hideProgressDialog() {
        if (dialog != null) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            dialog = null;
        }
    }
}