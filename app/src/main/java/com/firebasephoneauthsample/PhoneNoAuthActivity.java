package com.firebasephoneauthsample;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebasephoneauthsample.customviews.CustomEditTextView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;

/**
 * Created by Vaibhav Katkar on 08-Sep-17.
 */

public class PhoneNoAuthActivity extends AppCompatActivity implements PhoneNoAuthContract.View, CustomEditTextView.IWellthyEditText
{
    PhoneNoAuthPresenter mPresenter;

    @BindView(R.id.edtxtvwMobNo)
    CustomEditTextView edtxtvwMobNo;
    @BindView(R.id.txtvwCountryCode)
    TextView txtvwCountryCode;

    @BindView(R.id.btnVerifyMobNo)
    Button btnVerifyMobNo;
    @BindView(R.id.linlayPhnoLayout)
    LinearLayout linlayPhnoLayout;
    @BindView(R.id.linlayPassCodeLayout)
    LinearLayout linlayPassCodeLayout;
    @BindView(R.id.edtxtvwCode1)
    CustomEditTextView edtxtvwCode1;
    @BindView(R.id.edtxtvwCode2)
    CustomEditTextView edtxtvwCode2;
    @BindView(R.id.edtxtvwCode3)
    CustomEditTextView edtxtvwCode3;
    @BindView(R.id.edtxtvwCode4)
    CustomEditTextView edtxtvwCode4;
    @BindView(R.id.edtxtvwCode5)
    CustomEditTextView edtxtvwCode5;
    @BindView(R.id.edtxtvwCode6)
    CustomEditTextView edtxtvwCode6;

    @BindView(R.id.txtvwSmsSent)
    TextView txtvwSmsSent;
    @BindView(R.id.chckBoxResearch)
    CheckBox chckBoxResearch;
    @BindView(R.id.edtxtIdToken)
    CustomEditTextView edtxtIdToken;


    String idToken;
    private FirebaseAuth mAuth;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    private String mVerificationId = "";
    FirebaseUser user;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phno_auth);
        ButterKnife.bind(this);
        setupViews();
        mPresenter = new PhoneNoAuthPresenter(this);
    }

    private void setupViews()
    {
        chckBoxResearch.setText("Terms and Conditions");
        chckBoxResearch.setClickable(true);
        chckBoxResearch.setMovementMethod(LinkMovementMethod.getInstance());
        chckBoxResearch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked)
            {
                if (isChecked)
                {

                } else
                {
                    //showResearchConfirmationDialog();
                }
            }
        });

    }

    @OnClick(R.id.btnVerifyMobNo)
    public void onVerifyMobNoClicked()
    {
        if (mPresenter != null)
        {
            mPresenter.validateMobNo(edtxtvwMobNo.getText().toString().trim());
        }
    }

    @Override
    public void setLoadingIndicator(boolean status)
    {
        if (status)
        {
            Utilities.showProgressDialog(this, getString(R.string.please_wait));
        } else
        {
            Utilities.hideProgressDialog();
        }
    }

    @Override
    public void initFirebaseLogin()
    {
        mAuth = FirebaseAuth.getInstance();
        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks()
        {

            @Override
            public void onVerificationFailed(FirebaseException e)
            {
                // This callback is invoked in an invalid request for verification is made,
                // for instance if the the phone number format is not valid.
                Timber.e("onVerificationFailed" + e);
                setLoadingIndicator(false);
                if (e instanceof FirebaseAuthInvalidCredentialsException)
                {
                    Toast.makeText(PhoneNoAuthActivity.this, "FirebaseAuthInvalidCredentialsException - " + e.toString(), Toast.LENGTH_LONG).show();
                    // Invalid request
                    // ...
                } else if (e instanceof FirebaseTooManyRequestsException)
                {
                    Toast.makeText(PhoneNoAuthActivity.this, "FirebaseTooManyRequestsException - " + e.toString(), Toast.LENGTH_LONG).show();
                    // The SMS quota for the project has been exceeded
                    // ...
                } else if (e instanceof FirebaseAuthInvalidUserException)
                {
                    Toast.makeText(PhoneNoAuthActivity.this, "FirebaseAuthInvalidUserException - " + e.toString(), Toast.LENGTH_LONG).show();
                }

                // Show a message and update the UI
                // ...
            }

            @Override
            public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential)
            {
                // This callback will be invoked in two situations:
                // 1 - Instant verification. In some cases the phone number can be instantly
                //     verified without needing to send or enter a verification code.
                // 2 - Auto-retrieval. On some devices Google Play services can automatically
                //     detect the incoming verification SMS and perform verificaiton without
                //     user action
                Timber.i("onVerificationCompleted:" + phoneAuthCredential);

                setLoadingIndicator(false);
            }

            @Override
            public void onCodeSent(String verificationId, PhoneAuthProvider.ForceResendingToken token)
            {
                // The SMS verification code has been sent to the provided phone number, we
                // now need to ask the user to enter the code and then construct a credential
                // by combining the code with a verification ID.
                Timber.d("onCodeSent:" + verificationId);
                setLoadingIndicator(false);

                // Save verification ID and resending token so we can use them later
                mVerificationId = verificationId;
                mResendToken = token;
                showPassCodeLayout();
            }
        };
    }

    @Override
    public void authenticateWithFirebase()
    {
        setLoadingIndicator(true);
        PhoneAuthProvider.getInstance().verifyPhoneNumber(txtvwCountryCode.getText() + edtxtvwMobNo.getText().toString().trim(),        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks);        // OnVerificationStateChangedCallbacks
    }

    @Override
    public void showPassCodeLayout()
    {
        txtvwSmsSent.setText(getString(R.string.otp_sent_msg, edtxtvwMobNo.getText().toString()));
        initCodeListeners();
        linlayPhnoLayout.setVisibility(View.GONE);
        linlayPassCodeLayout.setVisibility(View.VISIBLE);
        //startResendTimer();
    }

    @Override
    public void showPhnoLayout()
    {
        linlayPhnoLayout.setVisibility(View.VISIBLE);
        linlayPassCodeLayout.setVisibility(View.GONE);
    }

    @Override
    public void signInWithFirebase()
    {
        new Timer().schedule(new TimerTask()
        {
            @Override
            public void run()
            {
                PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, getEnteredPassCode());
                String strProvider = credential.getProvider();
                signInWithPhoneAuthCredential(credential);
            }
        }, 1000);
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential)
    {
        mAuth.signInWithCredential(credential).addOnCompleteListener(this, new OnCompleteListener<AuthResult>()
        {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task)
            {
                if (task.isSuccessful())
                {
                    // Sign in success, update UI with the signed-in user's information
                    Timber.e("signInWithCredential:success");

                    user = task.getResult().getUser();
                    final String phno = task.getResult().getUser().getPhoneNumber();
                    user.getIdToken(true).addOnCompleteListener(new OnCompleteListener<GetTokenResult>()
                    {
                        public void onComplete(@NonNull Task<GetTokenResult> task)
                        {
                            if (task.isSuccessful())
                            {
                                idToken = task.getResult().getToken();
                                Timber.i("FIREBASE_ID_TOKEN : " + idToken);
                                edtxtIdToken.setText(idToken);
                                // Send token to your backend via HTTPS
                                // ...
                            } else
                            {
                                Toast.makeText(PhoneNoAuthActivity.this,getString(R.string.login_failed),Toast.LENGTH_SHORT).show();

                                // Handle error -> task.getException();
                            }
                        }
                    });
                    // ...
                } else
                {
                    // Sign in failed, display a message and update the UI
                    Timber.e("signInWithCredential:failure" + task.getException());
                    if (task.getException() instanceof FirebaseAuthInvalidCredentialsException)
                    {
                        // The verification code entered was invalid
                        Toast.makeText(PhoneNoAuthActivity.this,getString(R.string.passcode_expired),Toast.LENGTH_SHORT).show();
                        //showResendButton();
                    }
                }
            }

        });
    }

    private String getEnteredPassCode()
    {
        return edtxtvwCode1.getText().toString().trim() + edtxtvwCode2.getText().toString().trim() + edtxtvwCode3.getText().toString().trim() + edtxtvwCode4.getText().toString().trim() + edtxtvwCode5.getText().toString().trim() + edtxtvwCode6.getText().toString().trim();

    }

    @Override
    public void showSignInFailed()
    {

    }

    @Override
    public void showSignInSucessful()
    {

    }

    @Override
    public void backPressed(int id)
    {
        Timber.d("Should now request focus on id " + id);
        CustomEditTextView currentView;
        currentView = (CustomEditTextView) findViewById(id);
        if (currentView != null)
        {
            currentView.requestFocus();
            currentView.setSelection(currentView.getText().toString().length());
        }
    }

    private void initCodeListeners()
    {
        edtxtvwCode1.addTextChangedListener(new PhoneNoAuthActivity.BackHandler(edtxtvwCode1, edtxtvwCode2, null, false));
        edtxtvwCode2.addTextChangedListener(new PhoneNoAuthActivity.BackHandler(edtxtvwCode2, edtxtvwCode3, edtxtvwCode1, false));
        edtxtvwCode3.addTextChangedListener(new PhoneNoAuthActivity.BackHandler(edtxtvwCode3, edtxtvwCode4, edtxtvwCode2, false));
        edtxtvwCode4.addTextChangedListener(new PhoneNoAuthActivity.BackHandler(edtxtvwCode4, edtxtvwCode5, edtxtvwCode3, false));
        edtxtvwCode5.addTextChangedListener(new PhoneNoAuthActivity.BackHandler(edtxtvwCode5, edtxtvwCode6, edtxtvwCode4, false));
        edtxtvwCode6.addTextChangedListener(new PhoneNoAuthActivity.BackHandler(edtxtvwCode6, null, edtxtvwCode5, true));

        edtxtvwCode2.setBackPressedListener(this);
        edtxtvwCode3.setBackPressedListener(this);
        edtxtvwCode4.setBackPressedListener(this);
        edtxtvwCode5.setBackPressedListener(this);
        edtxtvwCode6.setBackPressedListener(this);

        edtxtvwCode1.requestFocus();
    }


    private class BackHandler implements TextWatcher
    {

        private CustomEditTextView current;
        @Nullable
        private CustomEditTextView next;
        @Nullable
        private CustomEditTextView previous;
        private boolean shouldValidateEntireCode;

        public BackHandler(CustomEditTextView current, CustomEditTextView next, CustomEditTextView previous, boolean shouldValidateEntireCode)
        {
            //            TODO assignments.
            this.current = current;
            this.next = next;
            this.previous = previous;
            this.shouldValidateEntireCode = shouldValidateEntireCode;

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after)
        {

        }

        @Override
        public void onTextChanged(@NonNull CharSequence s, int start, int before, int count)
        {
            if (s.toString().trim().length() == 1 && next != null)
            {
                next.requestFocus();
            } else if (count == 0 && previous != null)
            {
                //                This interferes with the back handling, by making the selected text box jump.
                //                previous.requestFocus();
            } else if (shouldValidateEntireCode && s.toString().trim().length() == 1)
            {
                //Analytics Event-Activation Code Entered Manually
                if (mPresenter != null)
                {
                    mPresenter.validatePassCode(edtxtvwCode1, edtxtvwCode2, edtxtvwCode3, edtxtvwCode4, edtxtvwCode5, edtxtvwCode6);
                }
            }
        }

        @Override
        public void afterTextChanged(@NonNull Editable arg0)
        {
            String s = arg0.toString();
            if (!s.equals(s.toUpperCase()))
            {
                s = s.toUpperCase();
                current.setText(s);
            }
        }
    }
}
