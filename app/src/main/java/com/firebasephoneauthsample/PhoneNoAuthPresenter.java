package com.firebasephoneauthsample;

import android.text.TextUtils;

import com.firebasephoneauthsample.customviews.CustomEditTextView;

import org.jetbrains.annotations.NotNull;


/**
 * Created by Vaibhav Katkar on 19-Aug-17.
 */

public class PhoneNoAuthPresenter implements PhoneNoAuthContract.Presenter
{
    private PhoneNoAuthContract.View view;

    private int MOB_NO_LENGHT = 10;

    PhoneNoAuthPresenter(PhoneNoAuthContract.View view)
    {
        this.view = view;
        view.initFirebaseLogin();
    }

    @Override
    public void setLoginView(String verificationId)
    {
        if(!"".equalsIgnoreCase(verificationId))
        {
            view.showPassCodeLayout();
        }
        else
        {
            view.showPhnoLayout();
        }
    }

    @Override
    public void authenticateMobNo(CustomEditTextView edtxtvwMobNo, PhoneNoAuthActivity activity)
    {
        view.authenticateWithFirebase();
    }

    @Override
    public void validateMobNo(@NotNull String strMobNo)
    {
        if (!TextUtils.isEmpty(strMobNo) && strMobNo.length() == MOB_NO_LENGHT) // Add the Validation Logic here later, when country code and Phone No is added.
        {
            view.authenticateWithFirebase();
        } else
        {
            // show error msg on view.
        }
    }

    @Override
    public void validatePassCode(CustomEditTextView edtxtvwCode1, CustomEditTextView edtxtvwCode2, CustomEditTextView edtxtvwCode3, CustomEditTextView edtxtvwCode4, CustomEditTextView edtxtvwCode5, CustomEditTextView edtxtvwCode6)
    {
        if (!edtxtvwCode1.getText().toString().equalsIgnoreCase("")) {
            if (!edtxtvwCode2.getText().toString().equalsIgnoreCase("")) {
                if (!edtxtvwCode3.getText().toString().equalsIgnoreCase("")) {
                    if (!edtxtvwCode4.getText().toString().equalsIgnoreCase("")) {
                        if (!edtxtvwCode5.getText().toString().equalsIgnoreCase("")) {
                            if (!edtxtvwCode6.getText().toString().equalsIgnoreCase("")) {
                                view.signInWithFirebase();
                            } else {
                                edtxtvwCode6.requestFocus();
                            }
                        } else {
                            edtxtvwCode5.requestFocus();
                        }
                    } else {
                        edtxtvwCode4.requestFocus();
                    }
                } else {
                    edtxtvwCode3.requestFocus();
                }
            } else {
                edtxtvwCode2.requestFocus();
            }
        } else {
            edtxtvwCode1.requestFocus();
        }
    }
}
