package com.firebasephoneauthsample;


import android.widget.EditText;

import com.firebasephoneauthsample.customviews.CustomEditTextView;

/**
 * Created by Vaibhav Katkar on 19-Aug-17.
 */

public class PhoneNoAuthContract
{
    interface View {
        void setLoadingIndicator(boolean status);

        /**
         * Method that initialize the Firebase Login.
         */
        void initFirebaseLogin();

        /**
         * Method that starts the authentication process of Mob No. with firebase.
         */
        void authenticateWithFirebase();

        void showPassCodeLayout();

        void showPhnoLayout();

        void signInWithFirebase();


        void showSignInFailed();

        void showSignInSucessful();
    }

    interface Presenter {

        void setLoginView(String verificationId);
        void authenticateMobNo(CustomEditTextView edtxtvwMobNo, PhoneNoAuthActivity activity);

        void validateMobNo(String strMobNo);


        void validatePassCode(CustomEditTextView edtxtvwCode1, CustomEditTextView edtxtvwCode2, CustomEditTextView edtxtvwCode3, CustomEditTextView edtxtvwCode4, CustomEditTextView edtxtvwCode5, CustomEditTextView edtxtvwCode6);
    }

}
