package com.firebasephoneauthsample.customviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.EditText;
import com.firebasephoneauthsample.R;
import timber.log.Timber;


public class CustomEditTextView extends EditText
{

    private IWellthyEditText wellthyEditTextInterface;
    private boolean clearFocusOnKeyboardDrop = false;


    public CustomEditTextView(Context context, @NonNull AttributeSet attrs, int defStyleAttr, int defStyleRes)
    {
        super(context, attrs, defStyleAttr, defStyleRes);
        setCustomAttributes(attrs);
    }

    public CustomEditTextView(Context context, @NonNull AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        setCustomAttributes(attrs);
        if (!isInEditMode())
        {
            int style = Typeface.NORMAL;
            String i = attrs.getAttributeValue("http://schemas.android.com/apk/res/android", "textStyle");

            if (i != null)
            {
                style = Integer.parseInt(i.replace("0x", ""));
            }
            init(context, style);
        }
    }

    public CustomEditTextView(Context context, @NonNull AttributeSet attrs)
    {
        super(context, attrs);
        setCustomAttributes(attrs);
        if (!isInEditMode())
        {
            int style = Typeface.NORMAL;
            String i = attrs.getAttributeValue("http://schemas.android.com/apk/res/android", "textStyle");
            if (i != null)
            {
                style = Integer.parseInt(i.replace("0x", ""));
            }
            init(context, style);
        }
    }

    public CustomEditTextView(Context context)
    {
        super(context);
        if (!isInEditMode())
        {
            init(context, Typeface.NORMAL);
        }
    }

    private void init(Context ctx, int style)
    {
        //setTypeface(FontManager.getAppTextFont(ctx), style);
        // setTextSize(15);

    }

    @Override
    public boolean onKeyUp(int keyCode, @NonNull KeyEvent event)
    {
        boolean result = super.onKeyUp(keyCode, event);
        if (wellthyEditTextInterface != null)
        {
            if (event.getKeyCode() == KeyEvent.KEYCODE_DEL && event.getAction() == KeyEvent.ACTION_UP)
            {
                wellthyEditTextInterface.backPressed(getNextFocusLeftId());
                //                return false;
            }
        }

        return result;
    }

    public void setBackPressedListener(IWellthyEditText wellthyEditTextInterface)
    {
        this.wellthyEditTextInterface = wellthyEditTextInterface;
    }

    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event)
    {
        if (keyCode == KeyEvent.KEYCODE_BACK && clearFocusOnKeyboardDrop)
        {
            clearFocus();
        }
        return super.onKeyPreIme(keyCode, event);
    }

    private void setCustomAttributes(AttributeSet attrs)
    {

        if (attrs != null)
        {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.CustomEditTextView, 0, 0);
            try
            {
                clearFocusOnKeyboardDrop = a.getBoolean(R.styleable.CustomEditTextView_set_clear_focus_on_keyboard_drop, false);

            }
            finally
            {
                a.recycle();
            }
            Timber.d("Check :%b", clearFocusOnKeyboardDrop);

        }
    }

    public boolean isClearFocusOnKeyboardDrop()
    {
        return clearFocusOnKeyboardDrop;
    }

    public interface IWellthyEditText
    {

        void backPressed(int id);
    }
}